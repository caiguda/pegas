//
//  CTOdnoklassniki.m
//  Pegas
//
//  Created by Yuriy Bosov on 12/3/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTOdnoklassniki.h"
#import "CTSocialNetwork+Protected.h"
#import "NSDictionary+NullProtected.h"
#import "Odnoklassniki.h"

/**
 * Documentation
 * http://apiok.ru/wiki/pages/viewpage.action?pageId=79003783
 * http://apiok.ru/wiki/pages/viewpage.action?pageId=75989046
 **/


@interface CTOdnoklassniki () <OKSessionDelegate, OKRequestDelegate>
{
    NSArray* permissions;
}

@property (nonatomic, strong, readwrite) Odnoklassniki* okApi;

@end


@implementation CTOdnoklassniki

@synthesize okApi;

CT_IMPLEMENT_SINGLETON(CTOdnoklassniki);

- (void)setup
{
    NSDictionary* gpConfig = [[NSBundle mainBundle].infoDictionary objectForKey:@"Odnoklassniki"];

    redirectUrl = [gpConfig objectForKey:@"redirectUrl"];
    NSAssert(self.redirectUrl, @"Need redirect url!");

    NSString* permissionStr = [gpConfig objectForKey:@"permissions"];
    permissions = [permissionStr componentsSeparatedByString:@","];
    if (!permissions)
        permissions = @[ @"VALUABLE_ACCESS" ];

    NSString* appID = [gpConfig objectForKey:@"appID"];
    NSAssert(appID, @"Need appID!");
    NSString* appKey = [gpConfig objectForKey:@"appKey"];
    NSAssert(appKey, @"Need appKey!");
    NSString* appSecret = [gpConfig objectForKey:@"appSecret"];
    NSAssert(appSecret, @"Need appSecret!");
    okApi = [[Odnoklassniki alloc] initWithAppId:appID andAppSecret:appSecret andAppKey:appKey andDelegate:self];
}

- (BOOL)isAuthorized
{
    return okApi.isSessionValid;
}

- (NSString*)accessToken
{
    return okApi.session.accessToken;
}

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    [self registerCallback:aCallback forEvent:kCTSocialNetworkCallbackLogin];
    if (self.isAuthorized)
    {
        [okApi refreshToken];
    }
    else
    {
        [okApi authorize:permissions];
    }
}

- (void)logout
{
    [okApi logout];
}

// TODO: Implement userProfile

#pragma mark - OKSessionDelegate

- (void)okDidLogin
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:okApi.session.accessToken error:nil canceled:NO];
}

- (void)okDidNotLogin:(BOOL)canceled
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:nil canceled:canceled];
}

- (void)okDidNotLoginWithError:(NSError*)error
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:error canceled:NO];
}

- (void)okDidExtendToken:(NSString*)accessToken
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:nil canceled:NO];
}

- (void)okDidNotExtendToken:(NSError*)error
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:error canceled:NO];
}

#pragma mark - Protected

- (void)applicationDidBecomeActive:(NSNotification*)aNotification
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:nil canceled:YES];
}

- (void)applicationWillTerminate:(NSNotification*)aNotification
{
    [self logout];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;

    NSString* redirect = [self.redirectUrl lowercaseString];
    if ([[[url absoluteString] lowercaseString] hasPrefix:redirect])
    {
        result = [okApi.session handleOpenURL:url];
    }

    return result;
}

@end
