//
//  CTOdnoklassniki.h
//  Pegas
//
//  Created by Yuriy Bosov on 12/3/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"


@interface CTOdnoklassniki : CTSocialNetwork

CT_DECLARE_SINGLETON(CTOdnoklassniki);

@end
