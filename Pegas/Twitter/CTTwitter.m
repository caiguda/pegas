//
//  CTTwitter.m
//  Pegas
//
//  Created by Yuriy Bosov on 9/27/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTTwitter.h"
#import "CTSocialNetwork+Protected.h"
#import "CTUserProfile+Twitter.h"
#import "CTCore.h"
#import "TwitterViewController.h"
#import "STTwitter.h"
#import "ASATwitterUserAccount.h"
#import "ASATwitterCommunicator.h"

#import <Accounts/Accounts.h>

#define kCTTwitterOAuthTokenKey @"kCTTwitterOAuthTokenKey"
#define kCTTwitterOAuthTokenSecretKey @"kCTTwitterOAuthTokenSecretKey"
#define kCTTwitterUserIDKey @"kCTTwitterUserIDKey"
#define kCTTwitterScreenName @"kCTTwitterScreenName"


@interface CTTwitter ()
{
    STTwitterAPI* twitterAPI;
//    ASATwitterUserAccount* account;

    ACAccountStore* accountStore;
    ACAccount* account;
}

@end


@implementation CTTwitter

CT_IMPLEMENT_SINGLETON(CTTwitter);

@synthesize oauthToken, oauthTokenSecret;
@synthesize consumerKey, consumerSecret;

- (void)setup
{
    NSDictionary* gpConfig = [[NSBundle mainBundle].infoDictionary objectForKey:@"Twitter"];

    consumerKey = [gpConfig objectForKey:@"consumerKey"];
    NSParameterAssert(consumerKey);
    consumerSecret = [gpConfig objectForKey:@"consumerSecret"];
    NSParameterAssert(consumerSecret);

//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    account = [[ASATwitterUserAccount alloc] initWithToken:[defaults objectForKey:kCTTwitterOAuthTokenKey]
//                                               tokenSecret:[defaults objectForKey:kCTTwitterOAuthTokenSecretKey]
//                                             twitterUserID:[defaults objectForKey:kCTTwitterUserIDKey]
//                                            userScreenName:[defaults objectForKey:kCTTwitterScreenName]];
//    if (!self.isAuthorized)
//        account = nil;
    
    accountStore = [ACAccountStore new];
    account = nil;
}

- (BOOL)isAuthorized
{
    return account.credential.oauthToken.length;
//    return account.oauthToken.length &&
//        account.oauthTokenSecret.length &&
//        account.twitterUserID.length &&
//        account.screenName.length;
}

- (NSString*)accessToken
{
//    return self.oauthToken;
    return account.credential.oauthToken;
}

- (NSString*)oauthToken
{
//    return account.oauthToken;
    return account.credential.oauthToken;
}

- (NSString*)oauthTokenSecret
{
//    return account.oauthTokenSecret;
    return nil;
}

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    
    ACAccountType* accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error)
    {
        if(granted)
        {
            account = [accountStore accountsWithAccountType:accountType].firstObject;
            twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:account delegate:nil];
            dispatch_async(dispatch_get_main_queue(), ^
            {
                aCallback(account, error, NO);
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                aCallback(nil, error, !error);
            });
        }
    }];
    
//    __weak __typeof(&*self) weakSelf = self;
//    CTSocialNetworkCallback block = ^(id data, NSError* error, BOOL canceled)
//    {
//        if (data && !error && !canceled)
//        {
//            if ([data isKindOfClass:[ASATwitterUserAccount class]])
//            {
//                [weakSelf setupAccount:data];
//            }
//        }
//        
//        aCallback(data,error,canceled);
//    };
//
//    if (self.isAuthorized)
//    {
//        aCallback(account, nil, NO);
//    }
//    else
//    {
//        TwitterViewController* vc = [[TwitterViewController alloc] initWithCallBack:block];
//        if (CT_IS_IPAD)
//        {
//            vc.modalPresentationStyle = UIModalPresentationFormSheet;
//        }
//        UIViewController* parentController = (self.parentViewController) ? (self.parentViewController) : (CTGetPrimeViewController());
//        [parentController presentViewController:vc animated:YES completion:nil];
//    }
}

#pragma mark - Logout

- (void)logout
{
    account = nil;
    twitterAPI = nil;
//    [self clearAllTwitterCookies];
//    [self clearAccessToken];
}

//- (void)clearAllTwitterCookies
//{
//    NSHTTPCookieStorage* cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    for (NSHTTPCookie* cookie in [cookieStorage cookies])
//    {
//        if ([[cookie domain] rangeOfString:@"twitter.com"].location != NSNotFound)
//        {
//            [cookieStorage deleteCookie:cookie];
//        }
//    }
//}

//- (void)clearAccessToken
//{
//    account = nil;
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    [defaults removeObjectForKey:kCTTwitterOAuthTokenKey];
//    [defaults removeObjectForKey:kCTTwitterOAuthTokenSecretKey];
//    [defaults removeObjectForKey:kCTTwitterUserIDKey];
//    [defaults removeObjectForKey:kCTTwitterScreenName];
//    [defaults synchronize];
//}

//- (void)setupAccount:(ASATwitterUserAccount*)anAccount
//{
//    account = anAccount;
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:account.oauthToken forKey:kCTTwitterOAuthTokenKey];
//    [defaults setObject:account.oauthTokenSecret forKey:kCTTwitterOAuthTokenSecretKey];
//    [defaults setObject:account.twitterUserID forKey:kCTTwitterUserIDKey];
//    [defaults setObject:account.screenName forKey:kCTTwitterScreenName];
//    [defaults synchronize];
//}

#pragma mark - UserProfile

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    assert(aCallback);
//    void (^block)(ASATwitterUserAccount*) = ^(ASATwitterUserAccount* anAccount)
//    {
//        //[self setupAccount:anAccount];
//        
//        if (!twitterAPI)
//            twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:consumerKey consumerSecret:consumerSecret oauthToken:anAccount.oauthToken oauthTokenSecret:anAccount.oauthTokenSecret];
//        
//        [twitterAPI getUsersShowForUserID:anAccount.twitterUserID orScreenName:anAccount.screenName includeEntities:@(NO) successBlock:^(NSDictionary *userDictionary)
//         {
//             CTUserProfile* user = [CTUserProfile userProfileWithTwitterDictionary:userDictionary];
//             aCallback(user, nil, NO);
//         }
//         errorBlock:^(NSError *error)
//         {
//             aCallback(nil, error, NO);
//         }];
//    };

    void (^block)() = ^()
    {
//        if (!twitterAPI)
//            twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:account];
        
        [twitterAPI getUsersShowForUserID:account.identifier orScreenName:account.username includeEntities:@(NO) successBlock:^(NSDictionary *userDictionary)
         {
             CTUserProfile* user = [CTUserProfile userProfileWithTwitterDictionary:userDictionary];
             aCallback(user, nil, NO);
         }
        errorBlock:^(NSError *error)
         {
             aCallback(nil, error, NO);
         }];
    };

    if (self.isAuthorized)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError* error, BOOL cancelled)
        {
             if (!error && !cancelled)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
        }];
    }
}

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback;
{
    NSParameterAssert(aCallback);
    void (^block)() = ^()
    {
//        if (!twitterAPI)
//            twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:account];
        
        [twitterAPI getFollowersListForUserID:account.identifier
                                 orScreenName:nil
                                        count:@"10000"
                                       cursor:nil
                                   skipStatus:nil
                          includeUserEntities:nil
                                 successBlock:^(NSArray *users, NSString *previousCursor, NSString *nextCursor)
        {
            NSMutableArray* friends = nil;
            if (users.count)
            {
                friends = [NSMutableArray new];
                CTUserProfile* user;
                for (NSDictionary* dic in users)
                {
                    user = [CTUserProfile userProfileWithTwitterDictionary:dic];
                    [friends addObject:user];
                }
            }
            aCallback(friends, nil, NO);

        } errorBlock:^(NSError *error)
        {
            aCallback(nil, error, NO);
        }];
    };

    if (self.isAuthorized)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError* error, BOOL cancelled)
        {
             if (!error && !cancelled)
             {
                 block(data);
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
        }];
    }
}

- (void)inviteFriendWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    void (^block)() = ^()
    {
        NSString* message = [aPostData nullProtectedObjectForKey:kCTFeedMessageKey];
        NSString* link = [aPostData nullProtectedObjectForKey:kCTFeedLinkKey];
        NSString* userID = [aPostData nullProtectedObjectForKey:kCTInvitedUserIDKey];
        
//        if (!twitterAPI)
//            twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:account];

        NSMutableString* status = [NSMutableString stringWithString:@""];
        if (message)
        {
            status = [NSMutableString stringWithString:[message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
        if (link)
        {
            if ([status length])
                [status appendString:@"\n"];
            
            [status appendString:[link stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [twitterAPI postDirectMessage:status forScreenName:nil orUserID:userID successBlock:^(NSDictionary *message)
        {
            aCallback(message, nil, NO);
            
        } errorBlock:^(NSError *error)
        {
            aCallback(nil, error, NO);
        }];
    };

    if (self.isAuthorized)
    {
        block();
    }
    else
    {
        [self loginWithCallback:^(id data, NSError* error, BOOL cancelled)
        {
             if (!error && !cancelled)
             {
                 block();
             }
             else
             {
                 aCallback(data, error, cancelled);
             }
        }];
    }
}

@end
