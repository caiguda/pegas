//
//  CTUserProfile+Twitter.h
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile.h"


@interface CTUserProfile (Twitter)

+ (CTUserProfile*)userProfileWithTwitterDictionary:(NSDictionary*)aDictionary;
- (void)setupWithTwitterDictionary:(NSDictionary*)aDictionary;

@end
