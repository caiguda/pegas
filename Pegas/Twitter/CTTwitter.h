//
//  CTTwitter.h
//  Pegas
//
//  Created by Yuriy Bosov on 9/27/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"


@interface CTTwitter : CTSocialNetwork

CT_DECLARE_SINGLETON(CTTwitter);

@property (nonatomic, readonly) NSString* oauthToken;
@property (nonatomic, readonly) NSString* oauthTokenSecret;
@property (nonatomic, readonly) NSString* consumerKey;
@property (nonatomic, readonly) NSString* consumerSecret;

@property (nonatomic, weak) UIViewController* parentViewController;

@end
