//
//  CTVkontakte.h
//  Pegas
//
//  Created by Yuriy Bosov on 11/13/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//
#import "CTSocialNetwork.h"


@interface CTVkontakte : CTSocialNetwork

CT_DECLARE_SINGLETON(CTVkontakte);

@property (nonatomic, weak) UIViewController* parentViewController;

@end
