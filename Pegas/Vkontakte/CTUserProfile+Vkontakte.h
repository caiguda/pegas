//
//  CTUserProfile+Vkontakte.h
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile.h"


@interface CTUserProfile (Vkontakte)

+ (CTUserProfile*)userProfileWithVkontakteDictionary:(NSDictionary*)aDictionary;
- (void)setupWithVkontakteDictionary:(NSDictionary*)aDictionary;

@end
