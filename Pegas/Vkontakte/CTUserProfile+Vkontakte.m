//
//  CTUserProfile+Vkontakte.m
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile+Vkontakte.h"
#import "CTSocialNetwork+Protected.h"
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (Vkontakte)

+ (CTUserProfile*)userProfileWithVkontakteDictionary:(NSDictionary*)aDictionary
{
    CTUserProfile* user = [CTUserProfile new];
    [user setupWithVkontakteDictionary:aDictionary];
    return user;
}

- (void)setupWithVkontakteDictionary:(NSDictionary*)aDictionary
{
    self.userSocialNetwork = CTUserSocialNetworkVkontakte;
    self.sourceDictionary = aDictionary;

    self.ID = [NSString stringWithFormat:@"%@", [aDictionary nullProtectedObjectForKey:@"id"]];
    self.firstName = [aDictionary nullProtectedObjectForKey:@"first_name"];
    self.lastName = [aDictionary nullProtectedObjectForKey:@"last_name"];
    if (!self.name.length)
    {
        self.name = [CTSocialNetwork displayNameFromFirstName:self.firstName andLastName:self.lastName];
    }

    NSString* countryName = [((NSDictionary*)[aDictionary nullProtectedObjectForKey:@"country"])nullProtectedObjectForKey:@"title"];
    self.location = countryName;

    NSInteger languageCode = [[aDictionary nullProtectedObjectForKey:@"language"] integerValue];
    NSString* langArray[] =
    {
        @"ru",
        @"uk",
        @"be",
        @"en"
    };
    self.language = langArray[languageCode];

    NSInteger genderCode = [[aDictionary nullProtectedObjectForKey:@"sex"] integerValue];
    NSString* genders[] =
    {
        @"gemo",
        @"female",
        @"male"
    };
    self.gender = genders[genderCode];

    NSString* avatarPath = [aDictionary nullProtectedObjectForKey:@"photo"];
    if (avatarPath.length)
    {
        self.avatarURL = [NSURL URLWithString:avatarPath];
    }

    NSString* birthday = [aDictionary nullProtectedObjectForKey:@"bdate"];
    if (birthday.length)
    {
        NSDateFormatter* formatter = [NSDateFormatter new];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"dd.MM.yyyy";
        self.birthday = [formatter dateFromString:birthday];
    }
}

@end
