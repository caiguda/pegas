//
//  CTVkontakte.m
//  Pegas
//
//  Created by Yuriy Bosov on 11/13/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//


#import "CTVkontakte.h"
#import "CTSocialNetwork+Protected.h"
#import "CTUserProfile+Vkontakte.h"
#import "CTHelper.h"
#import "CTKitDefines.h"
#import "VKSdk.h"
#import "VKAuthorizeController.h"


@interface CTVkontakte () <VKSdkDelegate, VKSdkUIDelegate>
{
    NSArray* permissions;
}

@end


@implementation CTVkontakte

CT_IMPLEMENT_SINGLETON(CTVkontakte);

- (void)setup
{
    NSDictionary* gpConfig = [[NSBundle mainBundle].infoDictionary objectForKey:@"Vkontakte"];

    redirectUrl = [gpConfig objectForKey:@"redirectUrl"];
    NSAssert(self.redirectUrl, @"Need redirect url!");

    NSString* permissionStr = [gpConfig objectForKey:@"permissions"];
    permissions = [permissionStr componentsSeparatedByString:@","];
    NSAssert(permissions, @"Need permissions!");

    NSString* appID = [gpConfig objectForKey:@"appID"];
    NSAssert(appID, @"Need appID!");

    [VKSdk initializeWithAppId:appID];
    [[VKSdk instance] registerDelegate:self];
    [VKSdk instance].uiDelegate = self;
}

- (BOOL)isAuthorized
{
    return [VKSdk isLoggedIn];
}

- (NSString*)accessToken
{
    return [VKSdk accessToken].accessToken;
}

#pragma mark - Login/Logout

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    if (self.isAuthorized)
    {
        aCallback(self.accessToken, nil, NO);
    }
    else
    {
        [self registerCallback:aCallback forEvent:kCTSocialNetworkCallbackLogin];
        [VKSdk authorize:permissions];
    }
}

- (void)logout
{
    [VKSdk forceLogout];
}

#pragma mark - User profile

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    if (self.isAuthorized)
    {
        NSDictionary* params = @{ VK_API_ACCESS_TOKEN : [VKSdk accessToken].accessToken,
                                  VK_API_FIELDS : @"country,sex,bdate,lang,photo"
        };
        VKRequest* request = [[VKApi users] get:params];

        request.completeBlock = ^(VKResponse* response)
        {
            if ([response.json isKindOfClass:[NSArray class]])
            {
                NSArray* array = response.json;
                NSDictionary* dictionary = array.firstObject;
                CTUserProfile* user = [CTUserProfile userProfileWithVkontakteDictionary:dictionary];
                aCallback(user, nil, NO);
            }
            else
            {
                aCallback(nil, nil, NO);
            }
        };
        request.errorBlock = ^(NSError* error)
        {
            aCallback(nil, error, NO);
        };
        [request start];
    }
    else
    {
        __weak __typeof(&*self) weakSelf = self;
        [self loginWithCallback:^(id data, NSError* error, BOOL canceled)
        {
             if (data)
             {
                 [weakSelf userProfileWithCallback:aCallback];
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
        }];
    }
}

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    if (self.isAuthorized)
    {
        NSDictionary* params = @{    //VK_API_ACCESS_TOKEN : [VKSdk getAccessToken].accessToken,
            @"order" : @"name",
            VK_API_USER_ID : [VKSdk accessToken].userId,
            VK_API_FIELDS : @"country,sex,bdate,lang,photo"
        };
        VKRequest* request = [[VKApi friends] get:params];

        request.completeBlock = ^(VKResponse* response)
        {
            NSMutableArray* friends = nil;
            if ([response.json isKindOfClass:[NSDictionary class]])
            {
                NSDictionary* dic = response.json;
                NSArray* arr = [dic nullProtectedObjectForKey:@"items"];
                
                friends = [NSMutableArray new];
                CTUserProfile* user;
                for (NSDictionary* userDic in arr)
                {
                    user = [CTUserProfile userProfileWithVkontakteDictionary:userDic];
                    [friends addObject:user];
                }
            }
            aCallback(friends, nil, NO);
        };
        request.errorBlock = ^(NSError* error)
        {
            aCallback(nil, error, NO);
        };
        [request start];
    }
    else
    {
        __weak __typeof(&*self) weakSelf = self;
        [self loginWithCallback:^(id data, NSError* error, BOOL canceled)
        {
             if(data)
             {
                 [weakSelf friendsListWithCallback:aCallback];
             }
             else
             {
                 aCallback(nil, error, canceled);
             }
        }];
    }
}

#pragma mark - VKSdkDelegate

- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:result error:nil canceled:NO];
}

- (void)vkSdkUserAuthorizationFailed
{
    // ???
}

- (void)vkSdkUserAuthorizationFailed:(VKError *)result
{
    NSError* error = [NSError errorWithVkError:result];
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:error canceled:YES];
}

- (void)vkSdkAccessTokenUpdated:(VKAccessToken *)newToken oldToken:(VKAccessToken *)oldToken
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:newToken.accessToken error:nil canceled:NO];
}

#pragma mark - VKSdkUIDelegate

- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    UIViewController* parentController = (self.parentViewController) ? (self.parentViewController) : (CTGetPrimeViewController());
    [parentController presentViewController:controller animated:YES completion:nil];
}

- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    NSLog(@"VK: Need captcha error: %@", captchaError);
}

#pragma mark - Protected

- (void)applicationDidBecomeActive:(NSNotification*)aNotification
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:nil canceled:YES];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;

    NSString* redirect = [self.redirectUrl lowercaseString];
    if ([[[url absoluteString] lowercaseString] hasPrefix:redirect])
    {
        result = [VKSdk processOpenURL:url fromApplication:sourceApplication];
    }

    return result;
}

@end
