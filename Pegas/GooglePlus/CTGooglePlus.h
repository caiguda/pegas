//
//  CTGooglePlus.h
//  Pegas
//
//  Created by Yuriy Bosov on 20.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"


@interface CTGooglePlus : CTSocialNetwork

CT_DECLARE_SINGLETON(CTGooglePlus);

@property (nonatomic, readonly) NSString* serverAuthorizationCode;

- (void)disconnectWithCallback:(CTSocialNetworkCallback)aCallback;

@end
