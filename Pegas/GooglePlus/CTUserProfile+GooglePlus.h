//
//  CTUserProfile+GooglePlus.h
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile.h"

@class GPPSignIn;


@interface CTUserProfile (GooglePlus)

+ (CTUserProfile*)userProfileWithGPPSignIn:(GPPSignIn*)signIn;
- (void)setupWithGPPSignIn:(GPPSignIn*)signIn;

@end
