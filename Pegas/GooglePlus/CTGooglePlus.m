//
//  CTGooglePlus.m
//  Pegas
//
//  Created by Yuriy Bosov on 20.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGooglePlus.h"
#import "CTSocialNetwork+Protected.h"
#import "CTUserProfile+GooglePlus.h"
#import "NSDictionary+NullProtected.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

/**
 * See: https://developers.google.com/+/mobile/ios/sign-in
 **/

NSString* const kCTSocialNetworkCallbackDisconnect = @"kCTSocialNetworkCallbackDisconnect";


@interface CTGooglePlus () <GPPSignInDelegate>

@end


@implementation CTGooglePlus

CT_IMPLEMENT_SINGLETON(CTGooglePlus);

@dynamic serverAuthorizationCode;

- (void)setup
{
    [GPPSignIn sharedInstance].shouldFetchGooglePlusUser = YES;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserEmail = YES;
    [GPPSignIn sharedInstance].shouldFetchGoogleUserID = YES;

    NSDictionary* gpConfig = [[NSBundle mainBundle].infoDictionary objectForKey:@"GooglePlus"];

    redirectUrl = [gpConfig objectForKey:@"redirectURL"];
    NSAssert(self.redirectUrl, @"Need redirect url!");

    [GPPSignIn sharedInstance].clientID = [gpConfig objectForKey:@"clientID"];
    NSAssert([GPPSignIn sharedInstance].clientID, @"Need clientID!");

    [GPPSignIn sharedInstance].homeServerClientID = [gpConfig objectForKey:@"homeServerClientID"];
    [GPPSignIn sharedInstance].scopes = [NSArray arrayWithObjects:kGTLAuthScopePlusLogin, nil];
    [GPPSignIn sharedInstance].delegate = self;
}

- (BOOL)isAuthorized
{
    return [[GPPSignIn sharedInstance] trySilentAuthentication] && [GPPSignIn sharedInstance].authentication != nil;
}

- (NSString*)accessToken
{
    return [GPPSignIn sharedInstance].authentication.accessToken;
}

- (NSString*)serverAuthorizationCode
{
    return [GPPSignIn sharedInstance].homeServerAuthorizationCode;
}

#pragma mark - Login/Logout

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    if (self.isAuthorized)
    {
        aCallback(self.accessToken, nil, NO);
    }
    else
    {
        [self registerCallback:aCallback forEvent:kCTSocialNetworkCallbackLogin];
        [[GPPSignIn sharedInstance] authenticate];
    }
}

- (void)logout
{
    [[GPPSignIn sharedInstance] signOut];
}

- (void)disconnectWithCallback:(CTSocialNetworkCallback)aCallback
{
    [self registerCallback:aCallback forEvent:kCTSocialNetworkCallbackDisconnect];
    [[GPPSignIn sharedInstance] disconnect];
}

#pragma mark - User profile

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    if (self.isAuthorized)
    {
        CTUserProfile* user = [CTUserProfile userProfileWithGPPSignIn:[GPPSignIn sharedInstance]];
        aCallback(user, nil, NO);
    }
    else
    {
        __weak __typeof(&*self) weakSelf = self;
        [self loginWithCallback:^(id data, NSError* error, BOOL canceled)
        {
             if (data)
             {
                 [weakSelf userProfileWithCallback:aCallback];
             }
             else
             {
                 aCallback(nil, error, nil == error);
             }
        }];
    }
}

#pragma mark - google plus delegate

- (void)finishedWithAuth:(GTMOAuth2Authentication*)auth error:(NSError*)error
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin
                       withObject:auth.accessToken
                            error:error
                         canceled:!auth.accessToken && !error];
}

- (void)didDisconnectWithError:(NSError*)error
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackDisconnect withObject:nil error:error canceled:NO];
}

#pragma mark - Protected

- (void)applicationDidBecomeActive:(NSNotification*)aNotification
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackLogin withObject:nil error:nil canceled:YES];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;

    NSString* redirect = [self.redirectUrl lowercaseString];
    if ([[[url absoluteString] lowercaseString] hasPrefix:redirect])
    {
        result = [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    }

    return result;
}

@end
