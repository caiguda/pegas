//
//  CTUserProfile+GooglePlus.m
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile+GooglePlus.h"
#import "CTSocialNetwork+Protected.h"
#import <GooglePlus/GooglePlus.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (GooglePlus)

+ (CTUserProfile*)userProfileWithGPPSignIn:(GPPSignIn*)signIn
{
    CTUserProfile* user = [CTUserProfile new];
    [user setupWithGPPSignIn:signIn];
    return user;
}

- (void)setupWithGPPSignIn:(GPPSignIn*)signIn
{
    self.userSocialNetwork = CTUserSocialNetworkGooglePlus;

    self.ID = CT_NULL_PROTECT(signIn.googlePlusUser.identifier);
    self.email = (signIn.userEmail) ? (signIn.userEmail) : (signIn.authentication.userEmail);
    self.firstName = CT_NULL_PROTECT(signIn.googlePlusUser.name.givenName);
    self.lastName = CT_NULL_PROTECT(signIn.googlePlusUser.name.familyName);
    self.name = CT_NULL_PROTECT(signIn.googlePlusUser.displayName);
    if (!self.name.length)
    {
        self.name = [CTSocialNetwork displayNameFromFirstName:self.firstName andLastName:self.lastName];
    }
    self.login = CT_NULL_PROTECT(signIn.googlePlusUser.nickname);
    self.gender = CT_NULL_PROTECT(signIn.googlePlusUser.gender);
    self.language = CT_NULL_PROTECT(signIn.googlePlusUser.language);
    self.location = CT_NULL_PROTECT(signIn.googlePlusUser.currentLocation);

    NSString* avatarPath = CT_NULL_PROTECT(signIn.googlePlusUser.image.url);

    if (avatarPath.length)
    {
        self.avatarURL = [NSURL URLWithString:avatarPath];
    }

    NSString* birthday = [signIn.googlePlusUser birthday];
    if (birthday)
    {
        NSDateFormatter* formatter = [NSDateFormatter new];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"yyyy-MM-dd";
        self.birthday = [formatter dateFromString:birthday];
    }
}

@end
