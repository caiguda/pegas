//
//  CTUserProfile.h
//  Pegas
//
//  Created by Yuriy Bosov on 2/28/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum CTUserSocialNetwork
{
    CTUserSocialNetworkUnknown = 0,
    CTUserSocialNetworkFacebook,
    CTUserSocialNetworkTwitter,
    CTUserSocialNetworkGooglePlus,
    CTUserSocialNetworkVkontakte,
    CTUserSocialNetworkOdnoklasniki

} CTUserSocialNetwork;


@interface CTUserProfile : NSObject

@property (nonatomic, copy) NSString* ID;
@property (nonatomic, copy) NSString* email;
@property (nonatomic, copy) NSString* firstName;
@property (nonatomic, copy) NSString* lastName;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* login;
@property (nonatomic, copy) NSString* gender;
@property (nonatomic, strong) NSDate* birthday;
@property (nonatomic, strong) NSURL* avatarURL;
@property (nonatomic, copy) NSString* language;
@property (nonatomic, copy) NSString* location;
@property (nonatomic, assign) CTUserSocialNetwork userSocialNetwork;

@property (nonatomic, strong) NSDictionary* sourceDictionary;

- (NSString*)userSocialNetworkToString;

@end


@interface CTUserProfile (Archive) <NSCoding>

- (id)initWithCoder:(NSCoder*)aDecoder;
- (void)encodeWithCoder:(NSCoder*)aCoder;

@end
