//
//  CTUserProfile.m
//  Pegas
//
//  Created by Yuriy Bosov on 2/28/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTUserProfile.h"


NSString* const kXUserID = @"ID";
NSString* const kXUserEmail = @"email";
NSString* const kXUserFirstName = @"firstName";
NSString* const kXUserLastName = @"lastName";
NSString* const kXUserName = @"name";
NSString* const kXUserLogin = @"login";
NSString* const kXUserGender = @"gender";
NSString* const kXUserBirthday = @"birthday";
NSString* const kXUserAvatarURL = @"avatarURL";
NSString* const kXUserLanguage = @"language";
NSString* const kXUserLocation = @"location";
NSString* const kXUserSocialNetwork = @"userSocialNetwork";


@implementation CTUserProfile

- (NSString*)userSocialNetworkToString
{
    NSString* result = nil;
    switch (self.userSocialNetwork)
    {
        case CTUserSocialNetworkFacebook:
            result = @"FB";
            break;
        case CTUserSocialNetworkTwitter:
            result = @"TW";
            break;
        case CTUserSocialNetworkGooglePlus:
            result = @"GP";
            break;
        case CTUserSocialNetworkVkontakte:
            result = @"VK";
            break;
        case CTUserSocialNetworkOdnoklasniki:
            result = @"OD";
            break;
        default:
            result = @"unknown";
            break;
    }
    return result;
}

@end


@implementation CTUserProfile (Archive)

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super init];
    if (self)
    {
        self.ID = [aDecoder decodeObjectForKey:kXUserID];
        self.email = [aDecoder decodeObjectForKey:kXUserEmail];
        self.firstName = [aDecoder decodeObjectForKey:kXUserFirstName];
        self.lastName = [aDecoder decodeObjectForKey:kXUserLastName];
        self.name = [aDecoder decodeObjectForKey:kXUserName];
        self.login = [aDecoder decodeObjectForKey:kXUserLogin];
        self.gender = [aDecoder decodeObjectForKey:kXUserGender];
        self.birthday = [aDecoder decodeObjectForKey:kXUserBirthday];
        self.avatarURL = [aDecoder decodeObjectForKey:kXUserAvatarURL];
        self.language = [aDecoder decodeObjectForKey:kXUserLanguage];
        self.location = [aDecoder decodeObjectForKey:kXUserLocation];
        self.userSocialNetwork = (CTUserSocialNetwork)[aDecoder decodeIntegerForKey:kXUserSocialNetwork];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder*)aCoder
{
    [aCoder encodeObject:self.ID forKey:kXUserID];
    [aCoder encodeObject:self.email forKey:kXUserEmail];
    [aCoder encodeObject:self.firstName forKey:kXUserFirstName];
    [aCoder encodeObject:self.lastName forKey:kXUserLastName];
    [aCoder encodeObject:self.name forKey:kXUserName];
    [aCoder encodeObject:self.login forKey:kXUserLogin];
    [aCoder encodeObject:self.gender forKey:kXUserGender];
    [aCoder encodeObject:self.birthday forKey:kXUserBirthday];
    [aCoder encodeObject:self.avatarURL forKey:kXUserAvatarURL];
    [aCoder encodeObject:self.language forKey:kXUserLanguage];
    [aCoder encodeObject:self.location forKey:kXUserLocation];
    [aCoder encodeInteger:(NSInteger)self.userSocialNetwork forKey:kXUserSocialNetwork];
}

@end
