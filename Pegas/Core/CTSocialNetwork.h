//
//  CTSocialNetwork.h
//  Pegas
//
//  Created by Yuriy Bosov on 21.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <CaigudaKit/CTSingleton.h>


extern NSString* const kCTSocialNetworkCallbackLogin;
extern NSString* const kCTSocialNetworkCallbackUserProfile;
extern NSString* const kCTSocialNetworkCallbackFriends;
extern NSString* const kCTSocialNetworkCallbackInviteFriend;

extern NSString* const kCTFeedMessageKey;
extern NSString* const kCTFeedLinkKey;
extern NSString* const kCTFeedPictureURLKey;
extern NSString* const kCTFeedImage;
extern NSString* const kCTFeedNameKey;           //can only be used if kFeedLinkKey is specified
extern NSString* const kCTFeedDescriptionKey;    //can only be used if kFeedLinkKey is specified
extern NSString* const kCTInvitedUserIDKey;

typedef void (^CTSocialNetworkCallback)(id data, NSError* error, BOOL canceled);


@interface CTSocialNetwork : NSObject
{
    NSString* redirectUrl;
}

@property (nonatomic, readonly) NSString* accessToken;
@property (nonatomic, readonly) NSString* redirectUrl;

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback;
- (void)logout;
- (BOOL)isAuthorized;

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback;
- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback;
- (void)inviteFriendWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback;

// use this method in app delegate
+ (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation;

@end
