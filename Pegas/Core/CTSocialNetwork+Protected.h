//
//  CTSocialNetwork+Protected.h
//  PegasDemo
//
//  Created by Malaar on 18.07.15.
//  Copyright (c) 2015 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"


@interface CTSocialNetwork (Protected)

- (void)applicationDidFinishLaunching:(NSNotification*)aNotification;
- (void)applicationDidBecomeActive:(NSNotification*)aNotification;
- (void)applicationWillTerminate:(NSNotification*)aNotification;
- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation;

- (void)registerCallback:(CTSocialNetworkCallback)aCallback forEvent:(NSString*)anEvent;
- (void)executeCallbackForEvent:(NSString*)anEvent withObject:(id)anObject error:(NSError*)anError canceled:(BOOL)aCanceled;

+ (NSString*)displayNameFromFirstName:(NSString*)aFirstName andLastName:(NSString*)aLastName;

@end
