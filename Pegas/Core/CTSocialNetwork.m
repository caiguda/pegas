//
//  CTSocialNetwork.m
//  Pegas
//
//  Created by Yuriy Bosov on 21.11.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"
#import "CTSocialNetwork+Protected.h"

NSString* const kCTSocialNetworkCallbackLogin = @"kCTSocialNetworkCallbackLogin";
NSString* const kCTSocialNetworkCallbackUserProfile = @"kCTSocialNetworkCallbackUserProfile";
NSString* const kCTSocialNetworkCallbackFriends = @"kCTSocialNetworkCallbackFriends";
NSString* const kCTSocialNetworkCallbackInviteFriend = @"kCTSocialNetworkCallbackInviteFriend";

NSString* const kCTFeedMessageKey = @"message";
NSString* const kCTFeedLinkKey = @"link";
NSString* const kCTFeedPictureURLKey = @"picture";
NSString* const kCTFeedImage = @"image";
NSString* const kCTFeedNameKey = @"name";
NSString* const kCTFeedDescriptionKey = @"description";
NSString* const kCTInvitedUserIDKey = @"kCTInvitedUserIDKey";


static NSMutableArray* redirectURLInstances;


@interface CTSocialNetwork ()
{
    NSMutableDictionary* callbacks;
}

@end


@implementation CTSocialNetwork

@synthesize redirectUrl;
@dynamic accessToken;

- (id)init
{
    self = [super init];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidFinishLaunching:)
                                                     name:UIApplicationDidFinishLaunchingNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationWillTerminate:)
                                                     name:UIApplicationWillTerminateNotification
                                                   object:nil];
        callbacks = [NSMutableDictionary new];
        if (!redirectURLInstances)
        {
            redirectURLInstances = [NSMutableArray new];
        }

        [self setup];

        if (self.redirectUrl)
        {
            [redirectURLInstances addObject:self];
        }
        NSLog(@"CTSocialNetwork init instanse %@", NSStringFromClass([self class]));
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [redirectURLInstances removeObject:self];
}

- (void)setup
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (BOOL)isAuthorized
{
    NSAssert(nil, @"Override this method in subclasses!");
    return NO;
}

- (NSString*)accessToken
{
    NSAssert(nil, @"Override this method in subclasses!");
    return nil;
}

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)logout
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)postOnMyWallWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void)inviteFriendWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSAssert(nil, @"Override this method in subclasses!");
}

+ (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    for (CTSocialNetwork* instance in redirectURLInstances)
    {
        NSString* redirect = [instance.redirectUrl lowercaseString];
        if ([[url.absoluteString lowercaseString] hasPrefix:redirect])
        {
            return [instance application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
        }
    }
    return NO;
}

#pragma mark - Protected

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // override in subclasses
}

- (void)applicationDidBecomeActive:(NSNotification *)aNotification
{
    // override in subclasses
}

- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    // override in subclasses
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    NSAssert(nil, @"Override this method in subclasses!");
    return NO;
}

- (void)registerCallback:(CTSocialNetworkCallback)aCallback forEvent:(NSString*)anEvent
{
    [callbacks setObject:[aCallback copy] forKey:anEvent];
}

- (void)executeCallbackForEvent:(NSString*)anEvent withObject:(id)anObject error:(NSError*)anError canceled:(BOOL)aCanceled
{
    CTSocialNetworkCallback callback = [callbacks objectForKey:anEvent];
    if (callback)
    {
        callback(anObject, anError, aCanceled);
        [callbacks removeObjectForKey:anEvent];
    }
}

+ (NSString*)displayNameFromFirstName:(NSString*)aFirstName andLastName:(NSString*)aLastName
{
    NSMutableString* displayName = [NSMutableString stringWithString:aFirstName];
    if (aFirstName.length && aLastName.length)
        [displayName appendString:@" "];
    [displayName appendString:aLastName];
    return [displayName copy];
}

//- (void)loginAndExecuteSelectorOnCompletion:(SEL)aSelector withCallback:(CTSocialNetworkCallback)aCallback
//{
//    __weak __typeof(&*self) weakSelf = self;
//    [self loginWithCallback:^(id data, NSError *error, BOOL canceled)
//     {
//         if(data && [weakSelf isAuthorized])
//             [weakSelf performSelector:aSelector withObject:aCallback];
//         else
//             aCallback(data, error, canceled);
//     }];
//}

@end
