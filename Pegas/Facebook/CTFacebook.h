//
//  CTFacebook.h
//  Pegas
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTSocialNetwork.h"

extern NSString* const kCTFeedCaptionKey;    //can only be used if kFeedLinkKey is specified

/**
 * Useful links:
 * https://www.facebook.com/settings?tab=applications
 **/


@interface CTFacebook : CTSocialNetwork

CT_DECLARE_SINGLETON(CTFacebook);

@end
