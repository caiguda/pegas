//
//  CTUserProfile+Facebook.m
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile+Facebook.h"
#import "CTSocialNetwork+Protected.h"
#import "CTKitDefines.h"
#import "NSDictionary+NullProtected.h"


@implementation CTUserProfile (Facebook)

+ (CTUserProfile*)userProfileWithFacebookDictionary:(NSDictionary*)aDictionary
{
    CTUserProfile* user = [CTUserProfile new];
    [user setupWithFacebookDictionary:aDictionary];
    return user;
}

- (void)setupWithFacebookDictionary:(NSDictionary*)aDictionary
{
    self.userSocialNetwork = CTUserSocialNetworkFacebook;
    self.sourceDictionary = aDictionary;
    self.ID = [aDictionary nullProtectedObjectForKey:@"id"];
    self.email = [aDictionary nullProtectedObjectForKey:@"email"];
    self.firstName = [aDictionary nullProtectedObjectForKey:@"first_name"];
    self.lastName = [aDictionary nullProtectedObjectForKey:@"last_name"];
    self.name = [aDictionary nullProtectedObjectForKey:@"name"];
    if (!self.name.length)
    {
        self.name = [CTSocialNetwork displayNameFromFirstName:self.firstName andLastName:self.lastName];
    }
    self.login = [aDictionary nullProtectedObjectForKey:@"name"];
    self.gender = [aDictionary nullProtectedObjectForKey:@"gender"];

    NSString* locale = [aDictionary nullProtectedObjectForKey:@"locale"];
    if (locale)
    {
        NSRange range = [locale rangeOfString:@"_"];
        if (range.location != NSNotFound)
        {
            NSRange rangeByLang = NSMakeRange(0, range.location);
            NSRange rangeByLoc = NSMakeRange(range.location + 1, locale.length - range.location - 1);
            self.language = [locale substringWithRange:rangeByLang];
            self.location = [locale substringWithRange:rangeByLoc];
        }
    }

    NSString* avatarPath = CT_NULL_PROTECT([aDictionary valueForKeyPath:@"picture.data.url"]);
    if (avatarPath.length)
    {
        self.avatarURL = [NSURL URLWithString:avatarPath];
    }

    NSString* birthday = CT_NULL_PROTECT([aDictionary objectForKey:@"birthday"]);
    if (birthday.length)
    {
        NSDateFormatter* formatter = [NSDateFormatter new];
        formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        formatter.dateFormat = @"MM/dd/yyyy";
        self.birthday = [formatter dateFromString:birthday];
    }
}

@end
