//
//  CTFacebook.m
//  Pegas
//
//  Created by Yuriy Bosov on 2/25/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFacebook.h"
#import "CTSocialNetwork+Protected.h"
#import "CTUserProfile+Facebook.h"
#import "NSDictionary+NullProtected.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

NSString* const kCTFeedCaptionKey = @"caption";


@interface CTFacebook () <FBSDKAppInviteDialogDelegate>
{
    NSArray* permissions;
    FBSDKLoginManager* loginManager;
}

@end


@implementation CTFacebook

CT_IMPLEMENT_SINGLETON(CTFacebook);

- (void)setup
{
    redirectUrl = [[NSBundle mainBundle].infoDictionary objectForKey:@"FacebookAppID"];
    NSAssert(self.redirectUrl, @"Need redirect url!");
    redirectUrl = [NSString stringWithFormat:@"fb%@", self.redirectUrl];

    NSString* permissionStr = [[NSBundle mainBundle].infoDictionary objectForKey:@"FacebookPermissions"];
    permissions = [permissionStr componentsSeparatedByString:@","];
    if (!permissions)
        permissions = [NSArray arrayWithObjects:@"public_profile", @"email", nil];

    loginManager = [FBSDKLoginManager new];
    loginManager.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    [FBSDKProfile enableUpdatesOnAccessTokenChange:NO];
}

- (BOOL)isAuthorized
{
    return self.accessToken.length != 0;
}

- (NSString*)accessToken
{
    return [FBSDKAccessToken currentAccessToken].tokenString;
}

- (void)loginWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    UIViewController* controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    if(controller.presentedViewController)
        controller = controller.presentedViewController;
    // TODO: add suport of login with publish permissions. see here: logInWithPublishPermissions:fromViewController:handler:
    [loginManager logInWithReadPermissions:permissions fromViewController:controller handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
    {
#ifdef DEBUG
         for (NSString* permission in permissions)
         {
             if (![result.grantedPermissions containsObject:permission])
             {
                 NSLog(@"FB: Permission NOT granted: %@", permission);
             }
         }
#endif
         aCallback(result.token.tokenString, error, result.isCancelled);
    }];
}

- (void)logout
{
    [loginManager logOut];
}

- (void)userProfileWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    if (self.isAuthorized)
    {
        __weak __typeof(&*self) weakSelf = self;
        NSString* path = @"me?fields=first_name,last_name,name,gender,locale,email,birthday,link,location";
        FBSDKGraphRequest* request = [[FBSDKGraphRequest alloc] initWithGraphPath:path parameters:nil];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection* connection, id result, NSError* error)
        {
             if (result)
             {
                 CTUserProfile* user = [CTUserProfile userProfileWithFacebookDictionary:result];
                 [weakSelf avatarURLForUserID:nil withCallback:^(NSURL* avatarURL, NSError *error)
                  {
                      user.avatarURL = avatarURL;
                      aCallback(user, error, NO);
                  }];
             }
             else
             {
                 aCallback(result, error, NO);
             }
        }];
    }
    else
    {
        //        [self loginAndExecuteSelectorOnCompletion:@selector(userProfileWithCallback:) withCallback:aCallback];
        __weak __typeof(&*self) weakSelf = self;
        [self loginWithCallback:^(id data, NSError* error, BOOL canceled)
        {
             if (data && [weakSelf isAuthorized])
                 [weakSelf userProfileWithCallback:aCallback];
             else
                 aCallback(data, error, canceled);
        }];
    }
}

- (void)friendsListWithCallback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);

    if (self.isAuthorized)
    {
        if (![[FBSDKAccessToken currentAccessToken] hasGranted:@"user_friends"])
        {
            NSLog(@"FB: user_friends permissions is not granted!");
            NSError* error = [NSError errorWithDomain:@"CTFacebookDomain"
                                                 code:1
                                             userInfo:@{ NSLocalizedDescriptionKey : @"user_friends permissions is not granted!" }];
            aCallback(nil, error, NO);
        }
        else
        {
            NSDictionary* params = @{@"fields": @"picture,first_name,last_name,name,gender,locale,email,birthday,link,location"};
            FBSDKGraphRequest* request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/friends" parameters:params];
            [request startWithCompletionHandler:^(FBSDKGraphRequestConnection* connection, id result, NSError* error)
            {
                 NSMutableArray* friends = nil;
                 if ([result isKindOfClass:[NSDictionary class]])
                 {
                     friends = [NSMutableArray new];
                     CTUserProfile* user;
                     NSArray* array = [result nullProtectedObjectForKey:@"data"];
                     for (NSDictionary* dic in array)
                     {
                         user = [CTUserProfile userProfileWithFacebookDictionary:dic];
                         [friends addObject:user];
                     }
                 }
                 aCallback(friends, error, NO);
            }];
        }
    }
    else
    {
        //        [self loginAndExecuteSelectorOnCompletion:@selector(friendsListWithCallback:) withCallback:aCallback];
        __weak __typeof(&*self) weakSelf = self;
        [self loginWithCallback:^(id data, NSError* error, BOOL canceled)
        {
             if (data && [weakSelf isAuthorized])
                 [weakSelf friendsListWithCallback:aCallback];
             else
                 aCallback(data, error, canceled);
        }];
    }
}

/**
 * See: https://developers.facebook.com/docs/applinks
 * App Invites does not require Facebook Login.
 **/
- (void)inviteFriendWithData:(NSDictionary*)aPostData callback:(CTSocialNetworkCallback)aCallback
{
    NSParameterAssert(aCallback);
    NSParameterAssert(aPostData);

    [self registerCallback:aCallback forEvent:kCTSocialNetworkCallbackInviteFriend];
    FBSDKAppInviteContent* content = [FBSDKAppInviteContent new];
    content.appLinkURL = [aPostData nullProtectedObjectForKey:kCTFeedLinkKey];
    content.appInvitePreviewImageURL = [aPostData nullProtectedObjectForKey:kCTFeedPictureURLKey];
    
    UIViewController* controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    if(controller.presentedViewController)
        controller = controller.presentedViewController;
    [FBSDKAppInviteDialog showFromViewController:controller withContent:content delegate:self];
}

#pragma mark -

- (void)avatarURLForUserID:(NSString*)aUserID withCallback:(void (^)(NSURL* avatarURL, NSError* error))aCallback
{
    NSParameterAssert(aCallback);
    aUserID = (aUserID.length) ? (aUserID) : (@"me");

    NSString* path = [NSString stringWithFormat:@"%@/picture?type=large&redirect=false", aUserID];
    FBSDKGraphRequest* request = [[FBSDKGraphRequest alloc] initWithGraphPath:path parameters:nil];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection* connection, id result, NSError* error)
    {
         NSString* avatarPath = [result nullProtectedObjectForKeyPath:@"data.url"];
         NSURL* avatarURL = [NSURL URLWithString:avatarPath];
         aCallback(avatarURL, error);
    }];
}

#pragma mark - FBSDKAppInviteDialogDelegate

- (void)appInviteDialog:(FBSDKAppInviteDialog*)appInviteDialog didCompleteWithResults:(NSDictionary*)results
{
    BOOL canceled = [[results objectForKey:@"completionGesture"] isEqualToString:@"cancel"];
    id object = (canceled) ? (nil) : (results);
    [self executeCallbackForEvent:kCTSocialNetworkCallbackInviteFriend withObject:object error:nil canceled:canceled];
}

- (void)appInviteDialog:(FBSDKAppInviteDialog*)appInviteDialog didFailWithError:(NSError*)error
{
    [self executeCallbackForEvent:kCTSocialNetworkCallbackInviteFriend withObject:nil error:error canceled:NO];
}

#pragma mark - Protected

- (void)applicationDidFinishLaunching:(NSNotification*)aNotification
{
    [[FBSDKApplicationDelegate sharedInstance] application:[UIApplication sharedApplication]
                             didFinishLaunchingWithOptions:aNotification.userInfo];
}

- (void)applicationDidBecomeActive:(NSNotification*)aNotification
{
    [FBSDKAppEvents activateApp];
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;

    NSString* redirect = [self.redirectUrl lowercaseString];
    if ([[[url absoluteString] lowercaseString] hasPrefix:redirect])
    {
        result = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                openURL:url
                                                      sourceApplication:sourceApplication
                                                             annotation:annotation];
    }
    return result;
}

@end
