//
//  CTUserProfile+Facebook.h
//  Pegas
//
//  Created by Yuriy Bosov on 10/20/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTUserProfile.h"


@interface CTUserProfile (Facebook)

+ (CTUserProfile*)userProfileWithFacebookDictionary:(NSDictionary*)aDictionary;
- (void)setupWithFacebookDictionary:(NSDictionary*)aDictionary;

@end
