#! user/bin/ruby

#====================================================================

def ProcessIcons()
    
    if (ARGV.length == 0) || (ARGV.length > 2)
        puts "first argument - icon file name to process"
        puts "second argument - icon file name to process for iOS6 (than first argument - icon for iOS7)"
        return
    end
    
    if ARGV.length == 1
      ios7Icon = ARGV[0]
      ios6Icon = ARGV[0]
    else
      ios7Icon = ARGV[0]
      ios6Icon = ARGV[1]
    end
    
    if !File.exist?(ios7Icon)
        puts "File #{ios7Icon} doesn't exist!"
        return;
    end
    if !File.exist?(ios6Icon)
        puts "File #{ios6Icon} doesn't exist!"
        return;
    end

    if ARGV.length == 2
      puts("Processing files #{ios7Icon}, #{ios6Icon} ...")
    else
      puts("Processing file #{ios7Icon} ...")
    end
    
    # for iPhone, iOS 5-6
    system("convert -resize 57x57 '#{ios6Icon}' 'Icon-57.png'")
    system("convert -resize 114x114 '#{ios6Icon}' 'Icon-57@2x.png'")
    # for iPad, iOS 5-6
    system("convert -resize 72x72 '#{ios6Icon}' 'Icon-72.png'")
    system("convert -resize 144x144 '#{ios6Icon}' 'Icon-72@2x.png'")
    # for Spotlight, iOS 5-6
    system("convert -resize 50x50 '#{ios6Icon}' 'Icon-50.png'")
    system("convert -resize 100x100 '#{ios6Icon}' 'Icon-50@2x.png'")

    # for iPhone, iOS 7
    system("convert -resize 120x120 '#{ios7Icon}' 'Icon-60@2x.png'")
    # for iPad, iOS 7
    system("convert -resize 76x76 '#{ios7Icon}' 'Icon-76.png'")
    system("convert -resize 152x152 '#{ios7Icon}' 'Icon-76@2x.png'")
    # for Spotlight, iOS 7, iOS8
    system("convert -resize 40x40 '#{ios7Icon}' 'Icon-40.png'")
    system("convert -resize 80x80 '#{ios7Icon}' 'Icon-40@2x.png'")
    system("convert -resize 120x120 '#{ios7Icon}' 'Icon-40@3x.png'")
    # for Settings
    system("convert -resize 29x29 '#{ios7Icon}' 'Icon-29.png'")
    system("convert -resize 58x58 '#{ios7Icon}' 'Icon-29@2x.png'")
    
    # for iPhone6 Plus, iOS8
    system("convert -resize 87x87 '#{ios7Icon}' 'Icon-29@3x.png'")
    system("convert -resize 180x180 '#{ios7Icon}' 'Icon-60@3x.png'")
    
    # for Itunes
    system("convert -resize 512x512 '#{ios7Icon}' 'iTunesArtwork.png'")
    system("convert -resize 1024x1024 '#{ios7Icon}' 'iTunesArtwork@2x.png'")
    
    puts("Done.")
end

ProcessIcons()