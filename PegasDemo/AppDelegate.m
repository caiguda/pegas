//
//  AppDelegate.m
//  PegasDemo
//
//  Created by Malaar on 29.06.16.
//  Copyright © 2016 Caiguda. All rights reserved.
//

#import "AppDelegate.h"
#import "CTSocialsTestController.h"
#import "CTSocialNetwork.h"
#import "BFURL.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIViewController* vc = [CTSocialsTestController new];
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = nc;
    [self.window makeKeyAndVisible];
    return YES;
}

- (BOOL)application:(UIApplication*)application openURL:(NSURL*)url sourceApplication:(NSString*)sourceApplication annotation:(id)annotation
{
    BOOL result = NO;
    BFURL* appUrl = [BFURL URLWithInboundURL:url sourceApplication:sourceApplication];
    if ([appUrl appLinkData])
    {
        CTShowSimpleAlert(@"Received link", [appUrl.targetURL absoluteString]);
    }
    if (!result)
        result = [CTSocialNetwork application:application openURL:url sourceApplication:sourceApplication annotation:application];
    return result;
}

@end
