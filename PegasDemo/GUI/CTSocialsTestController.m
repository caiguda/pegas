//
//  ViewController.m
//  PegasDemo
//
//  Created by Yuriy Bosov on 2/24/14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTSocialsTestController.h"
#import "CTFacebook.h"
#import "CTVkontakte.h"
#import "CTTwitter.h"
#import "CTGooglePlus.h"
#import "CTUserProfile.h"

#define kCTSharedLink @"http://caiguda.com"
#define kCTSharedText @"Pegas is the best!"


@interface CTSocialsTestController ()

@end


@implementation CTSocialsTestController

- (instancetype)init
{
    self = [super initWithNibName:@"CTSocialsTestController" bundle:nil];
    if (self)
    {
        self.title = @"Pegas Demo";
    }
    return self;
}

- (UIRectEdge)edgesForExtendedLayout
{
    return UIRectEdgeNone;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Facebook

- (IBAction)didButtonFloginClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTFacebook sharedInstance] loginWithCallback:^(id data, NSError* error, BOOL canceled) {
        CTLog(@"DATA:\n%@", data);
        CTLog(@"ERROR:\n%@", error);
        
        [weakSelf hideActivity];

        if(data && !error)
        {
            CTShowSimpleAlert(@"Facebook", @"Login successfull!");
        }
        else if(!canceled)
        {
            CTShowSimpleAlert(@"Facebook error", error.localizedDescription);
        }
    }];
}

- (IBAction)didButtonFprofileClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTFacebook sharedInstance] userProfileWithCallback:^(id data, NSError* error, BOOL canceled) {
         CTLog(@"DATA:\n%@", data);
         CTLog(@"ERROR:\n%@", error);
         [weakSelf hideActivity];
         if(data && !error)
         {
             CTUserProfile* userProfile = (CTUserProfile*)data;
             CTShowSimpleAlert(@"Facebook profile", userProfile.sourceDictionary.description);
         }
         else if(!canceled)
         {
             CTShowSimpleAlert(@"Facebook error", error.localizedDescription);
         }
    }];
}

- (IBAction)didButtonFlogoutClicked:(id)sender
{
    [[CTFacebook sharedInstance] logout];
}

- (IBAction)didButtonFinviteClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    NSDictionary* params = @{ kCTFeedLinkKey : [NSURL URLWithString:@"https://fb.me/678245482319843"],
                              kCTFeedPictureURLKey : [NSURL URLWithString:@"http://caiguda.com/themes/theme2/static/images/logo.png"]
    };
    [[CTFacebook sharedInstance] inviteFriendWithData:params callback:^(id data, NSError* error, BOOL canceled) {
         CTLog(@"DATA:\n%@", data);
         CTLog(@"ERROR:\n%@", error);
         [weakSelf hideActivity];
         if(data && !error)
         {
             CTShowSimpleAlert(@"Facebook", @"Invite successful");
         }
         else if(!canceled)
         {
             CTShowSimpleAlert(@"Facebook error", error.localizedDescription);
         }
    }];
}

- (IBAction)didButtonFfriendsClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTFacebook sharedInstance] friendsListWithCallback:^(id data, NSError* error, BOOL canceled) {
         CTLog(@"DATA:\n%@", data);
         CTLog(@"ERROR:\n%@", error);
         [weakSelf hideActivity];

         if(data && !error)
         {
             NSArray* friends = (NSArray*)data;
             CTShowSimpleAlert(@"Facebook friends", [NSString stringWithFormat:@"%@", @(friends.count)]);
         }
         else if(!canceled)
         {
             CTShowSimpleAlert(@"Facebook error", error.localizedDescription);
         }
    }];
}

#pragma mark - Twitter

- (IBAction)didButtonTloginClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTTwitter sharedInstance] loginWithCallback:^(id data, NSError* error, BOOL canceled) {
        CTLog(@"DATA:\n%@", data);
        CTLog(@"ERROR:\n%@", error);
        [weakSelf hideActivity];
    }];
}

- (IBAction)didButtonTprofileClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTTwitter sharedInstance] userProfileWithCallback:^(id data, NSError* error, BOOL canceled) {
         CTLog(@"DATA:\n%@", data);
         CTLog(@"ERROR:\n%@", error);
         [weakSelf hideActivity];
    }];
}


- (IBAction)didButtonTlogoutClicked:(id)sender
{
    [[CTTwitter sharedInstance] logout];
}

#pragma mark - Vkontakte

- (IBAction)didButtonVloginClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [CTVkontakte sharedInstance].parentViewController = self;
    [[CTVkontakte sharedInstance] loginWithCallback:^(id data, NSError* error, BOOL canceled) {
        CTLog(@"DATA:\n%@", data);
        CTLog(@"ERROR:\n%@", error);
        [weakSelf hideActivity];
    }];
}

- (IBAction)didButtonVprofileClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [CTVkontakte sharedInstance].parentViewController = self;
    [[CTVkontakte sharedInstance] userProfileWithCallback:^(id data, NSError* error, BOOL canceled) {
         CTLog(@"DATA:\n%@", data);
         CTLog(@"ERROR:\n%@", error);
         [weakSelf hideActivity];
    }];
}


- (IBAction)didButtonVlogoutClicked:(id)sender
{
    [[CTVkontakte sharedInstance] logout];
}

- (IBAction)didButtonVfriendsClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTVkontakte sharedInstance] friendsListWithCallback:^(id data, NSError* error, BOOL canceled) {
        CTLog(@"DATA:\n%@", data);
        CTLog(@"ERROR:\n%@", error);
        [weakSelf hideActivity];
    }];
}

#pragma mark - GooglePlus

- (IBAction)didButtonGloginClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTGooglePlus sharedInstance] loginWithCallback:^(id data, NSError* error, BOOL canceled) {
        CTLog(@"DATA:\n%@", data);
        CTLog(@"ERROR:\n%@", error);
        [weakSelf hideActivity];
    }];
}

- (IBAction)didButtonGprofileClicked:(id)sender
{
    [self showActivity];
    __weak __typeof(&*self) weakSelf = self;

    [[CTGooglePlus sharedInstance] userProfileWithCallback:^(id data, NSError* error, BOOL canceled) {
         CTLog(@"DATA:\n%@", data);
         CTLog(@"ERROR:\n%@", error);
         [weakSelf hideActivity];
    }];
}

- (IBAction)didButtonGlogoutClicked:(id)sender
{
    [[CTGooglePlus sharedInstance] logout];
}

- (IBAction)didButtonGfriendsClicked:(id)sender
{
    // ...
}

#pragma mark - Common

- (NSDictionary*)inviteParams
{
    NSDictionary* params = @{ kCTFeedLinkKey : [NSURL URLWithString:@"https://itunes.apple.com/app/id916128406"],
                              kCTFeedPictureURLKey : [NSURL URLWithString:@"http://caiguda.com/themes/theme2/static/images/logo.png"]
    };
    return params;
}

@end
