Pod::Spec.new do |s|
    
	#root
		s.name     = 'Pegas'
		s.summary  = 'Social API for Facebook, Twitter, GooglePlus, Vkontakte, Odnoklassniki: login, fetch user profile, friends, invite friend, etc.'
		s.version  = '0.1.9'
		s.license   = { :type => 'Apache', :file => 'LICENSE.txt' }
	
		s.homepage = 'http://caiguda.com'
		s.author   = 'Caiguda Software Studio'
		s.source   = { :git => 'https://Malaar@bitbucket.org/caiguda/pegas.git', :tag => '0.1.9' }
	
	#platform
		s.platform = :ios, '7.0' 
		s.ios.deployment_target = '7.0'
	
	#build settings 
	  	s.requires_arc = true
  
  	#file patterns
		# s.source_files = 'Pegas/**/*.{h,m}'
	
	#subspecs
		s.subspec 'Core' do |core|
			core.dependency 'CaigudaKit'
			core.source_files = 'Pegas/Core/**/*.{h,m}'
	    core.resources = "Pegas/**/*.{jpeg,png,xib,nib,txt}"
	    end
		
		s.subspec 'Facebook' do |facebook|
			facebook.dependency 'Pegas/Core'
			facebook.dependency 'FBSDKCoreKit','~> 4.13.1'
			facebook.dependency 'FBSDKLoginKit','~> 4.13.1'
			facebook.dependency 'FBSDKShareKit','~> 4.13.1'
			facebook.source_files = 'Pegas/Facebook/**/*.{h,m}'
		end
		
		s.subspec 'Twitter' do |twitter|
			twitter.dependency 'Pegas/Core'
			twitter.dependency 'STTwitter'
			twitter.source_files = 'Pegas/Twitter/**/*.{h,m}'
		end
		
		s.subspec 'Vkontakte' do |vkontakte|
			vkontakte.dependency 'Pegas/Core'
			vkontakte.dependency 'VK-ios-sdk'
			vkontakte.source_files = 'Pegas/Vkontakte/**/*.{h,m}'
		end

    # s.subspec 'GooglePlus' do |googleplus|
    #   googleplus.dependency 'Pegas/Core'
    #   googleplus.source_files = 'Pegas/GooglePlus/**/*.{h,m}'
    #   googleplus.vendored_frameworks = 'Frameworks/GooglePlus.framework', 'Pegas/GooglePlus/Frameworks/GoogleOpenSource.framework'
    #   googleplus.resource = "Frameworks/GooglePlus.bundle"
    #   googleplus.framework = 'AssetsLibrary', 'CoreLocation', 'CoreMotion', 'CoreGraphics', 'CoreText', 'MediaPlayer', 'Security', 'SystemConfiguration', 'AddressBook'
    #   googleplus.xcconfig = { 'HEADER_SEARCH_PATHS' => '"${PODS_ROOT}/Pegas/GooglePlus/Frameworks/GoogleOpenSource.framework/Versions/A/Headers"' }
    # end

		s.subspec 'Odnoklassniki' do |odnoklassniki|
			odnoklassniki.dependency 'Pegas/Core'
			odnoklassniki.dependency 'odnoklassniki_ios_sdk'
			odnoklassniki.source_files = 'Pegas/Odnoklassniki/**/*.{h,m}'
		end
end
